<!doctype html>
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>PHP simulator</title>
    <meta name="description" content="Your competitor Fanpage monitoring tools and Content management for your pages. SocialONE automagically share posts for you through the day with RSS to Page, Scheduling..">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <!-- needs images, font... therefore can not be part of ui.css -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/shortcut.js" type="text/javascript"></script>
    <script src="js/ace.js" type="text/javascript"></script>
    <style type="text/css" media="screen">
    #editor { 
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
</style>
</head>
<boby>
<span class="pull-right font-weight-bold;" id="message" style="padding:5px;position: fixed;top:0px;right:0px;background-color:red;color:white"></span>
<div class="container">
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-sm-12">
			<form class="form-horizontal" onsubmit="return run()">
				<div class="form-group">
					<button class="btn btn-default" type="submit"> Run </button> Never end with php closing tag ?>
					<a href="javascript:" class="text-primary" onclick="switchView()">Switch View</a>
					<a href="curl.php" class="text-primary" target="_blank">Curl</a>
					<span class="pull-right"><b>Ctrl+1</b>: run code; <b>Ctrl+2</b>: switch view</span>
					
				</div>
			</form>
		</div>
	</div>
		
		<div class="col-sm-12" id="code-panel" style="min-height:410px">
				<div class="form-group" style="position: relative">
					<!-- <textarea class="form-control" name="code" rows="15"></textarea> -->
					<div id="editor" style="height:400px" accesskey="n"></div>
				</div>
			
		</div>
		<div class="col-sm-12" id="result">
		</div>
	
</div>
<script>
var editor  = null;
window.onbeforeunload = function (evt) {
	
  var message = 'Are you sure you want to leave?';
  if (typeof evt == 'undefined') {
    evt = window.event;
  }
  if (evt) {
    evt.returnValue = message;
  }
  return '';
}
	$(document).ready(function(){

		editor = ace.edit("editor");
	    editor.setTheme("ace/theme/monokai");
	    editor.getSession().setMode("ace/mode/php");
		setTimeout(function(){
			editor.setValue("<\?php date_default_timezone_set('Asia/Ho_Chi_Minh');\n\n",1);
			editor.focus();
		},500);
	});
	function run(){
		$('#message').show().html('Running');
		$.ajax({
			url: 'exec.php',
			dataType: 'text',
			type: 'post',
			data: {code: editor.getValue()},
			success: function(rs){
				$('#result').html('<pre>' + rs + '</pre>');
				$('#message').hide();
			},
			error: function(xhr, status, error){
				$('#message').hide();
				$('#result').html('Error: ' + xhr.responseText);
			}
		});
		return false;
	}
	function switchView(){
		if($('#code-panel').attr('class') == 'col-sm-6'){
			$('#code-panel').removeClass('col-sm-6').addClass('col-sm-12');
			$('#result').removeClass('col-sm-6').addClass('col-sm-12');
		}else{
			$('#code-panel').removeClass('col-sm-12').addClass('col-sm-6');
			$('#result').removeClass('col-sm-12').addClass('col-sm-6');
		}
		return false;
	}
	shortcut.add("Ctrl+2", function() {
        switchView();
    });
	shortcut.add("Ctrl+1", function() {
        run();
    });
</script>
</boby>
</html>